/*
 * This sketch sends a UDP message over ETH from a ESP32 device to a IP adress.
 * Destination IP adress, UDP port and messages are defined in a file
 *
 * Based on the arduino-esp32
 * examples ETH_LAN8720 and WiFiUDPClient from espressif.
 *
 * https://github.com/espressif/arduino-esp32/tree/master/libraries/WiFi examples
 *
 * And Arduino's State Change Detection (Edge Detection) for pushbuttons
 * example code: https://www.arduino.cc/en/Tutorial/StateChangeDetection
 *
 */

#define IPSHOWCTL "10.143.1.1"
#define UDPPORT 7000
#define COMPONENT "onschatbaar_cpt_2"
#define UDPMSGON "VITRINE5_AAN"
#define UDPMSGOFF "VITRINE5_UIT"
#define UDPMSGON1 "VITRINE6_AAN"
#define UDPMSGOFF1 "VITRINE6_UIT"
#define UDPMSGON2 "VITRINE7_AAN"
#define UDPMSGOFF2 "VITRINE7_UIT"
#define UDPMSGON3 "VITRINE8_AAN"
#define UDPMSGOFF3 "VITRINE8_UIT"

// #define BUTTON1 5
// #define BUTTON2 2
// #define BUTTON3 13
// #define BUTTON4 4

#include <ETH.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
//#include "vars.h"

// IP address to send UDP data to:
// either use the ip address of the server or
// a network broadcast address

// @todo: these values should be set via templating
const char * udpAddress = IPSHOWCTL;
const int udpPort = UDPPORT;
const char * udpMessageOn = UDPMSGON;
const char * udpMessageOff = UDPMSGOFF;
const char * udpMessageOn1 = UDPMSGON1;
const char * udpMessageOff1 = UDPMSGOFF1;
const char * udpMessageOn2 = UDPMSGON2;
const char * udpMessageOff2 = UDPMSGOFF2;
const char * udpMessageOn3 = UDPMSGON3;
const char * udpMessageOff3 = UDPMSGOFF3;

// this constant won't change:

const int buttonPin = 4;
const int buttonPin1 = 13;
const int buttonPin2 = 2;
const int buttonPin3 = 5;
// the pin that the pushbutton is attached to

// Variables will change:

int buttonPushCounter = 0;
int buttonPushCounter1 = 0;
int buttonPushCounter2 = 0;
int buttonPushCounter3 = 0;
// counter for the number of button presses

int buttonState = 0;
int buttonState1 = 0;
int buttonState2 = 0;
int buttonState3 = 0;
// current state of the button

int lastButtonState = 0;
int lastButtonState1 = 0;
int lastButtonState2 = 0;
int lastButtonState3 = 0;
// previous state of the button

long lastDebounceTime = 0;
long lastDebounceTime1 = 0;
long lastDebounceTime2 = 0;
long lastDebounceTime3 = 0;
// the last time the output pin was toggled

long debounceDelay = 100;
long debounceDelay1 = 100;
long debounceDelay2 = 100;
long debounceDelay3 = 100;
// the debounce time; increase if the output flickers

//The udp library class
WiFiUDP udp;

static bool eth_connected = false;

void WiFiEvent(WiFiEvent_t event)
{
  switch (event) {
    case SYSTEM_EVENT_ETH_START:
      Serial.println("ETH Started");
      //set eth hostname here
      ETH.setHostname("esp32-ethernet");
      break;
    case SYSTEM_EVENT_ETH_CONNECTED:
      Serial.println("ETH Connected");
      break;
    case SYSTEM_EVENT_ETH_GOT_IP:
      Serial.print("ETH MAC: ");
      Serial.print(ETH.macAddress());
      Serial.print(", IPv4: ");
      Serial.print(ETH.localIP());
      if (ETH.fullDuplex()) {
        Serial.print(", FULL_DUPLEX");
      }
      Serial.print(", ");
      Serial.print(ETH.linkSpeed());
      Serial.println("Mbps");
      eth_connected = true;
      break;
    case SYSTEM_EVENT_ETH_DISCONNECTED:
      Serial.println("ETH Disconnected");
      eth_connected = false;
      break;
    case SYSTEM_EVENT_ETH_STOP:
      Serial.println("ETH Stopped");
      eth_connected = false;
      break;
    default:
      break;
  }
}

void setup()
{
  // initialize the button pin as a input:
  pinMode(buttonPin, INPUT_PULLUP);
  pinMode(buttonPin1, INPUT_PULLUP);
  pinMode(buttonPin2, INPUT_PULLUP);
  pinMode(buttonPin3, INPUT_PULLUP);

  // initialize serial communication:
  Serial.begin(9600);

  delay(1000);

  WiFi.onEvent(WiFiEvent);
  ETH.begin();

  // *** SETUP ArduinoOTA ***
  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  // ArduinoOTA.setHostname("myesp32");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

  ArduinoOTA.begin();

  Serial.println("OTA Ready");
  Serial.print("IP address: ");
  Serial.println(ETH.localIP());
}

void loop() {

  if (eth_connected) {
    //only send data when connected

    ArduinoOTA.handle();
    buttonState = digitalRead(buttonPin);
    buttonState1 = digitalRead(buttonPin1);
    buttonState2 = digitalRead(buttonPin2);
    buttonState3 = digitalRead(buttonPin3);

    //filter out any noise by setting a time buffer
    if ((millis() - lastDebounceTime) > debounceDelay) {

      // compare the buttonState to its previous state
      if (buttonState != lastButtonState) {

        udp.beginPacket(udpAddress,udpPort);
        if (buttonState == HIGH) {
          // button went from ON to OFF:
          Serial.println(udpMessageOff);
          udp.printf(udpMessageOff);

        } else {
          // button went from OFF to ON:
          buttonPushCounter++;
          Serial.println(udpMessageOn);
          udp.printf(udpMessageOn);
        }
        udp.endPacket();
        lastDebounceTime = millis();

        lastButtonState = buttonState;

      }
    }

    //filter out any noise by setting a time buffer
    if ((millis() - lastDebounceTime1) > debounceDelay1) {

      // compare the buttonState to its previous state
      if (buttonState1 != lastButtonState1) {

        udp.beginPacket(udpAddress,udpPort);
        if (buttonState1 == HIGH) {
          // button went from ON to OFF:
          Serial.println(udpMessageOff1);
          udp.printf(udpMessageOff1);

        } else {
          // button went from OFF to ON:
          buttonPushCounter1++;
          Serial.println(udpMessageOn1);
          udp.printf(udpMessageOn1);
        }
        udp.endPacket();
        lastDebounceTime1 = millis();

        lastButtonState1 = buttonState1;

      }
    }

    //filter out any noise by setting a time buffer
    if ((millis() - lastDebounceTime2) > debounceDelay2) {

      // compare the buttonState to its previous state
      if (buttonState2 != lastButtonState2) {

        udp.beginPacket(udpAddress,udpPort);
        if (buttonState2 == HIGH) {
          // button went from ON to OFF:
          Serial.println(udpMessageOff2);
          udp.printf(udpMessageOff2);

        } else {
          // button went from OFF to ON:
          buttonPushCounter2++;
          Serial.println(udpMessageOn2);
          udp.printf(udpMessageOn2);
        }
        udp.endPacket();
        lastDebounceTime2 = millis();

        lastButtonState2 = buttonState2;

      }
    }

    //filter out any noise by setting a time buffer
    if ((millis() - lastDebounceTime3) > debounceDelay3) {

      // compare the buttonState to its previous state
      if (buttonState3 != lastButtonState3) {

        udp.beginPacket(udpAddress,udpPort);
        if (buttonState3 == HIGH) {
          // button went from ON to OFF:
          Serial.println(udpMessageOff3);
          udp.printf(udpMessageOff3);

        } else {
          // button went from OFF to ON:
          buttonPushCounter3++;
          Serial.println(udpMessageOn3);
          udp.printf(udpMessageOn3);
        }
        udp.endPacket();
        lastDebounceTime3 = millis();

        lastButtonState3 = buttonState3;

          // Serial.print("number of button pushes: ");
          // Serial.println(buttonPushCounter);
          // Serial.print("Seconds since boot: ");
          // Serial.println(millis()/1000);
        // Send the UDP message
      }
    }
  }
}
